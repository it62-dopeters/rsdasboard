<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('QUEST_DIFFICULTY_MAP', array(
    0 => array(
        "name" => "Novice",
        "color" => "#0fd40a"
    ),
    1 => array(
        "name" => "Intermediate",
        "color" => "#ffec0a"
    ),
    2 => array(
        "name" => "Experienced",
        "color" => "#ff800a"
    ),
    3 => array(
        "name" => "Master",
        "color" => "#de0203"
    ),
    4 => array(
        "name" => "Grandmaster",
        "color" => "#4e04d8"
    ),
    250 => array(
        "name" => "Special",
        "color" => "#004dff"
    )
));

define('SKILL_ID_TO_NAME', array(
    0 => "Attack",
    1 => "Defence",
    2 => "Strength",
    3 => "Constitution",
    4 => "Ranged",
    5 => "Prayer",
    6 => "Magic",
    7 => "Cooking",
    8 => "Woodcutting",
    9 => "Fletching",
    10 => "Fishing",
    11 => "Firemaking",
    12 => "Crafting",
    13 => "Smithing",
    14 => "Mining",
    15 => "Herblore",
    16 => "Agility",
    17 => "Thieving",
    18 => "Slayer",
    19 => "Farming",
    20 => "Runecrafting",
    21 => "Hunter",
    22 => "Construction",
    23 => "Summoning",
    24 => "Dungeoneering",
    25 => "Divination",
    26 => "Invention",
));

define('ELITE_SKILLS', array(
    "Invention",
));

define('ELITE_SKILL_EXP_MAP', array(
    1 => 0,
    2 => 830,
    3 => 1861,
    4 => 2902,
    5 => 3980,
    6 => 5126,
    7 => 6390,
    8 => 7787,
    9 => 9400,
    10 => 11275,
    11 => 13605,
    12 => 16372,
    13 => 19656,
    14 => 23546,
    15 => 28138,
    16 => 33520,
    17 => 39809,
    18 => 47109,
    19 => 55535,
    20 => 64802,
    21 => 77190,
    22 => 90811,
    23 => 106221,
    24 => 123573,
    25 => 143025,
    26 => 164742,
    27 => 188893,
    28 => 215651,
    29 => 245196,
    30 => 277713,
    31 => 316311,
    32 => 358547,
    33 => 404634,
    34 => 454796,
    35 => 509259,
    36 => 568254,
    37 => 632019,
    38 => 700797,
    39 => 774834,
    40 => 854383,
    41 => 946227,
    42 => 1044569,
    43 => 1149696,
    44 => 1261903,
    45 => 1381488,
    46 => 1508756,
    47 => 1644015,
    48 => 1787581,
    49 => 1939773,
    50 => 2100917,
    51 => 2283490,
    52 => 2476369,
    53 => 2679907,
    54 => 2894505,
    55 => 3120508,
    56 => 3358307,
    57 => 3608290,
    58 => 3870846,
    59 => 4146374,
    60 => 4435275,
    61 => 4758122,
    62 => 5096111,
    63 => 5449685,
    64 => 5819299,
    65 => 6205407,
    66 => 6608473,
    67 => 7028964,
    68 => 7467354,
    69 => 7924122,
    70 => 8399751,
    71 => 8925664,
    72 => 9472665,
    73 => 10041285,
    74 => 10632061,
    75 => 11245538,
    76 => 11882262,
    77 => 12542789,
    78 => 13227679,
    79 => 13937496,
    80 => 14672812,
    81 => 15478994,
    82 => 16313404,
    83 => 17176661,
    84 => 18069395,
    85 => 18992239,
    86 => 19945833,
    87 => 20930821,
    88 => 21947856,
    89 => 22997593,
    90 => 24080695,
    91 => 25259906,
    92 => 26475754,
    93 => 27728955,
    94 => 29020233,
    95 => 30350318,
    96 => 31719944,
    97 => 33129852,
    98 => 34580790,
    99 => 36073511,
    100 => 37608773,
    101 => 39270442,
    102 => 40978509,
    103 => 42733789,
    104 => 44537107,
    105 => 46389292,
    106 => 48291180,
    107 => 50243611,
    108 => 52247435,
    109 => 54303504,
    110 => 56412678,
    111 => 58575823,
    112 => 60793812,
    113 => 63067521,
    114 => 65397835,
    115 => 67785643,
    116 => 70231841,
    117 => 72737330,
    118 => 75303019,
    119 => 77929820,
    120 => 80618654
));

define('SKILL_EXP_MAP', array(
    1 => 0,
    2 => 83,
    3 => 174,
    4 => 276,
    5 => 388,
    6 => 512,
    7 => 650,
    8 => 801,
    9 => 969,
    10 => 1154,
    11 => 1358,
    12 => 1584,
    13 => 1833,
    14 => 2107,
    15 => 2411,
    16 => 2746,
    17 => 3115,
    18 => 3523,
    19 => 3973,
    20 => 4470,
    21 => 5018,
    22 => 5624,
    23 => 6291,
    24 => 7028,
    25 => 7842,
    26 => 8740,
    27 => 9730,
    28 => 10824,
    29 => 12031,
    30 => 13363,
    31 => 14833,
    32 => 16456,
    33 => 18247,
    34 => 20224,
    35 => 22406,
    36 => 24815,
    37 => 27473,
    38 => 30408,
    39 => 33648,
    40 => 37224,
    41 => 41171,
    42 => 45529,
    43 => 50339,
    44 => 55649,
    45 => 61512,
    46 => 67983,
    47 => 75127,
    48 => 83014,
    49 => 91721,
    50 => 101333,
    51 => 111945,
    52 => 123660,
    53 => 136594,
    54 => 150872,
    55 => 166636,
    56 => 184040,
    57 => 203254,
    58 => 224466,
    59 => 247886,
    60 => 273742,
    61 => 302288,
    62 => 333804,
    63 => 368599,
    64 => 407015,
    65 => 449428,
    66 => 496254,
    67 => 547953,
    68 => 605032,
    69 => 668051,
    70 => 737627,
    71 => 814445,
    72 => 899257,
    73 => 992895,
    74 => 1096278,
    75 => 1210421,
    76 => 1336443,
    77 => 1475581,
    78 => 1629200,
    79 => 1798808,
    80 => 1986068,
    81 => 2192818,
    82 => 2421087,
    83 => 2673114,
    84 => 2951373,
    85 => 3258594,
    86 => 3597792,
    87 => 3972294,
    88 => 4385776,
    89 => 4842295,
    90 => 5346332,
    91 => 5902831,
    92 => 6517253,
    93 => 7195629,
    94 => 7944614,
    95 => 8771558,
    96 => 9684577,
    97 => 10692629,
    98 => 11805606,
    99 => 13034431,
    100 => 14391160,
    101 => 15889109,
    102 => 17542976,
    103 => 19368992,
    104 => 21385073,
    105 => 23611006,
    106 => 26068632,
    107 => 28782069,
    108 => 31777943,
    109 => 35085654,
    110 => 38737661,
    111 => 42769801,
    112 => 47221641,
    113 => 52136869,
    114 => 57563718,
    115 => 63555443,
    116 => 70170840,
    117 => 77474828,
    118 => 85539082,
    119 => 94442737,
    120 => 104273167,
    121 => 200000000
));
