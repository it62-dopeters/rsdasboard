<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

class Graph extends CI_Controller {
	public function index(){
		$this->load->model("user");
		$this->load->model("graph_model");
		$data = $this->graph_model->GenerateGraphData($this->user->getUserName());
		$this->load->view('layouts/header');      
		$this->load->view("graph", $data);
		$this->load->view('layouts/footer');
      }
}  
?>
