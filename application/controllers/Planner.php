<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Planner extends CI_Controller
{
    public function index()
    {
       $this->load->view('layouts/header');
       $this->load->view('planner');
       $this->load->view('layouts/footer');
    }
    public function add(){
	    # Get user
	    $this->load->model("user");
	    $user = $this->user->getUserId();
	    if(!isset($user)){
	            # User not logged in
		    print "Error. User is not set! Are you logged in?\n";
exit;
	    }
	    
	    # Now get all the dailies the user currently has active
	    $active_dailies = array();
	    $this->db->select("*");
	    $this->db->from("player_dailies");
	    $this->db->join("users", "player_dailies.pd_user = users.id");
	    $this->db->where("pd_user", $user);
	    $this->db->where("pd_active", "Y");
	    $query = $this->db->get();
	    foreach($query->result() as $row){
	    	$active_dailies[$row->pd_name] = $row->pd_repetition;
	    }
	    
	    # Analyze the sent data
	    $daily_data = json_decode($this->input->post("dailies"), true);
	    if(!isset($daily_data)){
	    	# TODO Error!
		print "There was an error reading your dailies! Please try again!";
		exit;
	    }
	    $to_insert = array();
	    # Iterate over the dailies
	    foreach($daily_data["dailies"] as $daily){
		    # First off we sanitize the reptition input
		    $before = $daily["repetitions"];
		    preg_replace('/\D/', '',  $daily["repetitions"]);
		    # if before != now then print out info!! XXX TODO
		    $daily["repetitions"] = (int) $daily["repetitions"];
		    
		    if(!isset($active_dailies[$daily["name"]])){
		        # If the data is not present in the db yet, add it to the insert batch
			array_push($to_insert, array("pd_name" => $daily["name"], "pd_repetition" => $daily["repetitions"]));
		    	print "<pre>DAILY NOT ACTIVE:\n";
			print_r($daily);
		    	print "</pre><br>";
		    }
		    elseif($active_dailies[$daily["name"]] !== $daily["repetitions"]){
		    	# The user apparently only wants to change the repetitions of the daily
		    }
		    else{
		    	print "<pre>DAILY ACTIVE!\n";
			print_r($daily);
			print "\n => \$active_dailies[ " . $daily["name"] . "] = " . $active_dailies[$daily["name"]] . "<pre><br>";
		    }
	    }
	    # Now that we iterated over the submitted data we can insert the new data into the database
	    # But only if there is stuff to insert!
	    if($to_insert !== array()){
		    $this->db->insert_batch($to_insert);
	    }
	    else{
	    	# print out info that nothing was changed / added whatever
		    print "No changes have been made.";
	    }
    }
}
