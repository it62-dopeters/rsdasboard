<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Quests extends CI_Controller
{
    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->model('user');
        $this->load->model("quests_model");
        $data["table"] = $this->quests_model->Get_Quest_List($this->user->getUsername());
        $this->load->view('quests', $data);
        $this->load->view('layouts/footer');
    }
    public function crawl($player = ''){
        $this->load->library("RS_Crawler", array("name" => $player));
    }

    public function test($player = ''){
        
        $this->load->library("Player", array("name" => $player));
        $this->player->Init();
        print "<pre>";
        print_r($this->player->Calculate_Skill_Progress());
        print "</pre>";
        
    }
    
}
