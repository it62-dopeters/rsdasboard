<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Rsdashboard extends CI_Controller {
      public function index(){

$this->load->model('user');
$this->load->library("Player", array("name" => $this->user->getUsername()));
$this->player->Init();

   
$data["skill_ov"] = '';
            foreach ($this->player->Calculate_Skill_Progress() as $skill => $werte) {
                  $data["skill_ov"] .= '

      					<div class="row mdown">
                            <div class="col-xl-12">
                                    <div class="skill-body text-white skill-bright">
                                    	<div class="row sbar">

                                          <div class="col-4 col-xl-3">
                                                <figure class="item-plate">
                                                      <img class="skill-icon" src="' . base_url() . 'css/images/' . $skill .'.png">
                                                </figure>
                                                <a class="skill-desc mobile-hide"> ' . $skill . ' </a>
                                          </div>

                                          <div class=" col-2 col-xl-1 padl">
                                          		<a   class="skill-lvl"> ' . $werte['current_level'] . ' </a>
                                          </div>

								  		<div class="col-5 col-xl-7 padl">
								  			<div id="progress-bar_' . $skill .'" class="progress-bar-row" data-percent="' . $werte['next_percent'] . '">
								  			</div>
				                  		</div>

                                          <div class="col-1 col-xl-1 mobile-hide">
                                        		<a class="skill-percent"> ' . $werte['next_percent']*100 . '% </a>
                                          </div>

                                        </div>
                                    </div>
                            </div>
                    	</div>';
      }
	    $this->load->view('layouts/header');
	    $this->load->view("rsdashboard", $data);
	    $this->load->view('layouts/footer');
      }
}
?>