<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Player
{
    protected $data = array();
    protected $clan = '';
    protected $name = '';
    protected $API;
    protected $CI;

    public function __construct($params = NULL)
    {
        if (isset($params['name'])) {
            $this->name = $params['name'];
        }
        $this->CI =& get_instance();
        if(isset($this->CI->player->name)){
            # This is a subclass calling a constructor, use the already set player name
            $this->name = $this->CI->player->name;
        }
    }
    public function Init($full = NULL){
        if(!isset($this->CI)){
            return;
        }
        $this->CI->load->library("RS_API");
        $this->CI->load->library("player/Skill.php");
        $this->CI->load->library("player/Quest.php");
        $this->CI->load->library("player/Activity.php");
        $this->CI->skill->Load();
        $this->CI->quest->Load();
        # Change to $this->CI->quest->Load_From_API();
        # if you want to load directly from the API! Not advised
        $this->CI->activity->Load();
        if($full == true){
            $data = $this->CI->rs_api->API_Request($this->CI->rs_api->SKILL_URL . "?user=" . $this->name . "&activities=0");
            $json_data = json_decode($data, true);
            # This is the data we want to keep in the internal object
            $keepThis = array(
                "rank",
                "combatlevel",
                "magic",
                "ranged",
                "melee",
                "questsstarted",
                "questscomplete",
                "questsnotstarted",
                "totalxp",
                "totalskill",
            );
            for ($i = 0; $i < count($keepThis); $i++) {
                if (isset($json_data[$keepThis[$i]])) {
                    $this->data[$keepThis[$i]] = $json_data[$keepThis[$i]];
                }
            }
            $link = "http://services.runescape.com/m=website-data/playerDetails.ws?names=%5B%22" . $this->name . "%22%5D&callback=jQuery000000000000000_0000000000";
            $data = $this->CI->rs_api->API_Request($link);
            // $data = string, JSON parsable!
            preg_match('/({.*?})/', $data, $matches);
            $json_data = json_decode($matches[0], true);
            $this->data["title"] = (isset($json_data["title"]) ? $json_data["title"] : '');
            $this->data["isSuffix"] = (isset($json_data["isSuffix"]) ? $json_data["isSuffix"] : 0);
            $this->data["recruiting"] = (isset($json_data["recruiting"]) ? $json_data["recruiting"] : 0);
            $this->clan = (isset($json_data["clan"]) ? $json_data["clan"] : "");
        }
    }

    public function Get($val){
        return $this->$val;
    }
    public function Get_Quests()
    {
        return $this->CI->quest->Get("quests");
    }
    public function Get_Skills()
    {
        return $this->CI->skill->Get("skills");
    }
    public function Get_Activities()
    {
        return $this->CI->activity->Get("activities");;
    }

    public function Write_Player_Skills_To_DB($force_all = false){
        $skills = $this->Get_Skills();
        if(!isset($skills)){
            print "Skills not set!<br>";
            return false;
        }
        $to_insert = array();
        $missing_counter = 0;
        if($force_all == true){
            foreach ($skills as $skill => $val) {
                $data = array(
                    "psp_user" => strtolower($this->name),
                    "psp_exp" => (int)($val["xp"] / 10),
                    "psp_level" => ($val["level"] == 99 ? $this->Get_Virtual_Level_By_Exp((int) ($val["xp"] / 10)) : $val["level"]),
                    "psp_skill" => $val["id"],
                );
                array_push($to_insert, $data);
                $missing_counter++;
            }
        }
        else{
            # first load the latest skill data so we don't insert unnecessary trash
            $this->CI->db->select("psp_skill, max(psp_level) AS psp_level, max(psp_exp) AS psp_exp, max(psp_insert_date) AS psp_insert_date");
            $this->CI->db->from('player_skillprogress');
            $this->CI->db->where("psp_user", strtolower($this->name));
            $this->CI->db->group_by("psp_skill");
            $query = $this->CI->db->get();
            $progress = array();
            foreach ($query->result() as $row) {
                $progress[$row->psp_skill] = $row->psp_exp;
            }
            # Now check if all the quests from the API are in the DB
            foreach ($skills as $skill => $val) {
                if(!isset($progress[$val["id"]]) || (int)$progress[$val["id"]] < $val["xp"]){
                    $data = array(
                        "psp_user" => strtolower($this->name),
                        "psp_exp" => (int)($val["xp"] / 10),
                        "psp_level" => ($val["level"] == 99 ? $this->Get_Virtual_Level_By_Exp((int) ($val["xp"] / 10)) : $val["level"]),
                        "psp_skill" => $val["id"],
                    );
                    array_push($to_insert, $data);
                    $missing_counter++;
                }
            }
        }
        if ($missing_counter > 0) {
            $this->CI->db->insert_batch("player_skillprogress", $to_insert);
            return true;
        }
    }
    
    public function Write_Player_Quests_To_DB(){
        $quests = $this->Get_Quests();
        if(!isset($quests)){
            return false;
        }
        $db_quests = $this->CI->quest->Load_From_DB();

        $status_to_id_mapper = array(
            "COMPLETED" => 1,
            "STARTED" => 2,
            "STARTABLE" => 3,
            "NOT_STARTABLE" => 4
        );
        
        $to_insert = array();
        foreach($db_quests as $id => $data){
            $status = $quests[$data["que_title"]]["status"];
            if($quests[$data["que_title"]]["status"] == "NOT_STARTED"){
                if($quests[$data["que_title"]]["userEligible"] == 1){
                    $status = "STARTABLE";
                }
                else{
                    $status = "NOT_STARTABLE";
                }
            }
            $to_insert[$id] = $status_to_id_mapper[$status];
        }
        $to_insert = array(
            "pqp_data" => json_encode($to_insert),
            "pqp_user" => strtolower($this->name)
        );
        $this->CI->db->insert("player_questprogress", $to_insert);
    }
    public function Calculate_Skill_Progress(){
        $skills = $this->Get_Skills();
        if(!isset($skills)){
            return array();
        }
        $result = array();
        foreach($skills as $skill => $val){
            $is_elite = $this->Is_Skill_Elite($skill);
            $cur_level = ($val["level"] == 99 ? $this->Get_Virtual_Level_By_Exp((int) ($val["xp"] / 10), $is_elite) : $val["level"]);
            $cur_exp = (int)($val["xp"] / 10);
            # Runescape API transmits values after comma w/o comma
            # Not confirmed: RS API sometimes returns exp values w/o comma values?
            $map = ($is_elite ? ELITE_SKILL_EXP_MAP : SKILL_EXP_MAP);
            $next_level_exp = $map[$cur_level + 1];
            $cur_level_exp = $map[$cur_level];
            $diff = $cur_exp - $cur_level_exp;
            $level_diff = $next_level_exp - $cur_level_exp;
            $next_percent = $diff / $level_diff * 100;
            $result[$skill] = array(
                "current_level" => $cur_level,
                "current_exp" => $cur_exp,
                "next_exp" => $next_level_exp,
                "next_percent" => number_format($next_percent / 100, 2), # Format the percentage so 1 = 100%, 0.5 = 50% etc.
            );
        }
        return $result;
    }

    private function Is_Skill_Elite($skill){
        return in_array($skill, ELITE_SKILLS);
    }

    private function Get_Virtual_Level_By_Exp($exp, $is_elite = false){
        $cur_level = 99;
        $map = ($is_elite ? ELITE_SKILL_EXP_MAP : SKILL_EXP_MAP);
        if($exp > $map[100] -1){
            if($exp > $map[120]-1){
                return 120;
            }
            else{
                # exp exceeds level 100 exp, calculate actual level
                while($map[$cur_level+1] < $exp && $cur_level < 119){
                    $cur_level++;
                }
            }
        }
        return $cur_level;
    }

    private function Calculate_Next_Level_By_Level($level) {
        $a=0;
        for($x=1; $x<$level; $x++) {
          $a += floor($x+300*pow(2, ($x/7)));
        }
        return floor($a/4);
    }
    /*
    This doesn't work yet, I have to find out a fomula for this first?!
    private function Calculate_Next_Level_By_Exp($exp) {
        $a=0;
        #
        for($x=1; $x<$level; $x++) {
          $a += floor($x+300*pow(2, ($x/7)));
        }
        return floor($a/4);
    }
    */
}
