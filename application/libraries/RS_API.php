<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RS_API
{
    public $API_URL = 'https://apps.runescape.com/runemetrics';
    public $QUEST_URL = "https://apps.runescape.com/runemetrics/quests";
    public $SKILL_URL = "https://apps.runescape.com/runemetrics/profile/profile";
    public $RUNEHQ_URL = "https://www.runehq.com";
    private $curl;
    public function __construct($params = '')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if (isset($params["proxy"]["IP"])) {
            curl_setopt($curl, CURLOPT_PROXY, $params["proxy"]["IP"]);
        }
        if (isset($params["proxy"]["Port"])) {
            curl_setopt($curl, CURLOPT_PROXYPORT, $params["proxy"]["Port"]);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 50);
        curl_setopt($curl, CURLOPT_TIMEOUT, 50);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $this->curl = $curl;
    }

    public function API_Request($url = '', $params = '')
    {
        // TODO Validate things
        #print "GET => '$url'<br>";
        curl_setopt($this->curl, CURLOPT_URL, $url);

        $ret = curl_exec($this->curl);
        return ($ret == NULL ? json_encode(array("error" => "No Internet Connection")) : $ret);
    }

    public function RAW_Request($url = '', $data = array()){
	curl_setopt($this->curl, CURLOPT_URL, $url);
	return curl_exec($this->curl);
    }
}
