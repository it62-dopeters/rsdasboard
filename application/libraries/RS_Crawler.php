<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RS_Crawler
{
    protected $CI;
    public function __construct($params = NULL)
    {
        if(isset($params["name"])){ # Only load the player if a name is supplied!
	    $this->CI =& get_instance();
            $this->CI->load->library("Player", $params);
            $this->CI->player->Init();
            $this->CI->player->Write_Player_Skills_To_DB(true);
            $this->CI->player->Write_Player_Quests_To_DB();

        }
    }

    public function Get_Quests_From_DB($key = "que_id"){
        # First select all Quests in DB
        $query = $this->CI->db->get("quests");
        $db_quests = array();
        foreach ($query->result() as $row) {
            $db_quests[$row->$key] = $row;
        }
        return $db_quests;
    }

    /**
     * @param $quest string Questname to load requirements for
     * @return array with requirements
     * TODO: Auslagern in eigene Datei
     */
    public function Load_Quest_Requirements($quest = '')
    {
        // NOTE:
        // Unstable Foundations is the beginner quest so has no id!
        $questname_to_runehqid = array(
            'Unstable Foundations' => 0,
            '\'Phite Club' => '1168',
            'All Fired Up' => '0956',
            'Animal Magnetism' => '0848',
            'Another Slice of H.A.M.' => '0875',
            'As A First Resort...' => '0922',
            'Back to my Roots' => '0905',
            'Back to the Freezer' => '1164',
            'Beneath Cursed Tides' => '1136',
            'Between a Rock...' => '0462',
            'Big Chompy Bird Hunting' => '0339',
            'Biohazard' => '0157',
            'Birthright of the Dwarves' => '1117',
            'The Blood Pact' => '1082',
            'Blood Runs Deep' => '1052',
            'The Branches of Darkmeyer' => '1098',
            'Bringing Home the Bacon' => '1116',
            'The Brink of Extinction' => '1112',
            'Broken Home' => '1127',
            'Buyers and Cellars' => '1083',
            'Cabin Fever' => '0665',
            'Call of the Ancestors' => '1135',
            'Carnillean Rising' => '1106',
            'Catapult Construction' => '0923',
            'Children of Mah' => '1159',
            'Chosen Commander' => '0979',
            'Clock Tower' => '0287',
            'A Clockwork Syringe' => '1095',
            'Cold War' => '0858',
            'Contact!' => '0854',
            'Cook\'s Assistant' => '0170',
            'Creature of Fenkenstrain' => '0445',
            'Crocodile Tears' => '1166',
            'Curse of Arrav' => '0992',
            'The Darkness of Hallowvale' => '0768',
            'Deadliest Catch' => '1096',
            'Dealing with Scabaras' => '0915',
            'The Death of Chivalry' => '0154',
            'Death Plateau' => '0414',
            'Death to the Dorgeshuun' => '0734',
            'Defender of Varrock' => '0963',
            'Demon Slayer' => '0181',
            'Desert Treasure' => '0480',
            'Devious Minds' => '0639',
            'Diamond in the Rough' => '1109',
            'Dig Site' => '0354',
            'Dimension of Disaster' => '1130',
            'Dishonour among Thieves' => '1129',
            'Do No Evil' => '1091',
            'Dragon Slayer' => '0146',
            'Dream Mentor' => '0876',
            'Druidic Ritual' => '0185',
            'Dwarf Cannon' => '0184',
            'Eadgar\'s Ruse' => '0429',
            'Eagles\' Peak' => '0840',
            'The Elder Kiln' => '1103',
            'The Elemental Workshop I' => '0348',
            'The Elemental Workshop II' => '0802',
            'The Elemental Workshop III' => '1085',
            'The Elemental Workshop IV' => '1094',
            'Enakhra\'s Lament' => '0649',
            'Enlightened Journey' => '0829',
            'Ernest the Chicken' => '0168',
            'Evil Dave\'s big day out' => '1173',
            'The Eyes of Glouphrie' => '0761',
            'Fairy Tale I - Growing Pains' => '0680',
            'Fairy Tale II - Cure a Queen' => '0750',
            'Fairy Tale III - Battle at Orks Rift' => '1084',
            'Family Crest' => '0332',
            'Fate of the Gods' => '1121',
            'The Feud' => '0469',
            'Fight Arena' => '0191',
            'The Firemaker\'s Curse' => '1101',
            'Fishing Contest' => '0162',
            'Forgettable Tale of a Drunken Dwarf' => '0556',
            'Forgiveness of a Chaos Dwarf' => '1012',
            'Fremennik Isles' => '0859',
            'The Fremennik Trials' => '0431',
            'Fur \'n\' Seek' => '1002',
            'Garden of Tranquility' => '0583',
            'Gertrude\'s Cat' => '0177',
            'Ghosts Ahoy' => '0449',
            'The Giant Dwarf' => '0512',
            'Glorious Memories' => '0980',
            'Goblin Diplomacy' => '0245',
            'The Golem' => '0478',
            'Gower Quest' => '1142',
            'The Grand Tree' => '0326',
            'Great Brain Robbery' => '0865',
            'Grim Tales' => '0880',
            'Gunnar\'s Ground' => '1089',
            'The Hand in the Sand' => '0933',
            'Haunted Mine' => '0437',
            'Hazeel Cult' => '0297',
            'Heart of Stone' => '1128',
            'Hero\'s Welcome' => '1132',
            'Heroes\' Quest' => '0333',
            'Holy Grail' => '0347',
            'Horror from the Deep' => '0432',
            'Hunt for Red Raktuber' => '0983',
            'Icthlarin\'s Little Helper' => '0481',
            'Imp Catcher' => '0190',
            'Impressing the Locals' => '1160',
            'In Aid of the Myreque' => '0694',
            'In Pyre Need' => '0975',
            'In Search of the Myreque' => '0441',
            'The Jack of Spades' => '1165',
            'Jungle Potion' => '0187',
            'Kennith\'s Concerns' => '0928',
            'Kindred Spirits' => '1141',
            'King of the Dwarves' => '1092',
            'King\'s Ransom' => '0893',
            'The Knight\'s Sword' => '0176',
            'Land of the Goblins' => '0906',
            'Legacy of Seergaze' => '0935',
            'Legends\' Quest' => '0244',
            'Let Them Eat Pie' => '1102',
            'The Light Within' => '1133',
            'The Lord of Vampyrium' => '1134',
            'Lost City' => '0189',
            'The Lost Tribe' => '0511',
            'Love Story' => '1087',
            'Lunar Diplomacy' => '0758',
            'Making History' => '0626',
            'Meeting History' => '0950',
            'Merlin\'s Crystal' => '0193',
            'The Mighty Fall' => '1124',
            'Missing My Mummy' => '0982',
            'Missing, Presumed Death' => '1118',
            'Monk\'s Friend' => '0186',
            'Monkey Madness' => '0435',
            'Mountain Daughter' => '0456',
            'Mourning\'s End Part I' => '0552',
            'Mourning\'s End Part II' => '0605',
            'Murder Mystery' => '0328',
            'My Arm\'s Big Adventure' => '0826',
            'Myths of the White Lands' => '0973',
            'Nature Spirit' => '0398',
            'Nomad\'s Elegy' => '1137',
            'Nomad\'s Requiem' => '1062',
            'Observatory Quest' => '0179',
            'Olaf\'s Quest' => '0873',
            'One of a Kind' => '1119',
            'One Piercing Note' => '1100',
            'One Small Favour' => '0451',
            'Our Man in the North' => '1167',
            'The Path of Glouphrie' => '0902',
            'Perils of Ice Mountain' => '0938',
            'Pirate\'s Treasure' => '0153',
            'Plague City' => '0155',
            'Plague\'s End' => '1125',
            'Priest in Peril' => '0373',
            'Prisoner of Glouphrie' => '1093',
            'Quiet Before the Swarm' => '1086',
            'Rag and Bone Man' => '0700',
            'Rat Catchers' => '0629',
            'Recipe for Disaster' => '0691',
            'Recruitment Drive' => '0536',
            'Regicide' => '0425',
            'The Restless Ghost' => '0173',
            'Ritual of the Mahjarrat' => '1099',
            'River of Blood' => '1140',
            'Rocking Out' => '0942',
            'Roving Elves' => '0446',
            'Royal Trouble' => '0721',
            'A Rum Deal' => '0613',
            'Rune Mechanics' => '1072',
            'Rune Memories' => '1111',
            'Rune Mysteries' => '0117',
            'Salt in the Wound' => '1097',
            'Scorpion Catcher' => '0314',
            'Sea Slug' => '0180',
            'Shades of Mort\'ton' => '0430',
            'Shadow of the Storm' => '0618',
            'A Shadow over Ashdale' => '1123',
            'Sheep Herder' => '0174',
            'Shield of Arrav' => '0247',
            'Shilo Village' => '0209',
            'Sliske\'s Endgame' => '1162',
            'Slug Menace' => '0774',
            'Smoking Kills' => '0941',
            'Some Like It Cold' => '1107',
            'Song from the Depths' => '1104',
            'A Soul\'s Bane' => '0697',
            'Spirit of Summer' => '0945',
            'Spirits of the Elid' => '0633',
            'Stolen Hearts' => '1108',
            'Summer\'s End' => '0961',
            'Swan Song' => '0711',
            'Swept Away' => '0965',
            'Tai Bwo Wannai Trio' => '0420',
            'A Tail of Two Cats' => '0602',
            'Tale of the Muspah' => '0981',
            'Tears of Guthix' => '0491',
            'The Temple at Senntisten' => '1042',
            'Temple of Ikov' => '0782',
            'Throne of Miscellania' => '0434',
            'TokTz-Ket-Dill' => '0939',
            'The Tourist Trap' => '0356',
            'Tower of Life' => '0864',
            'Tree Gnome Village' => '0188',
            'Tribal Totem' => '0145',
            'Troll Romance' => '0440',
            'Troll Stronghold' => '0417',
            'Underground Pass' => '0262',
            'Vampyre Slayer' => '0178',
            'A Void Dance' => '1088',
            'The Void Stares Back' => '1090',
            'Wanted!' => '0604',
            'The Watchtower' => '0341',
            'Waterfall Quest' => '0238',
            'What Lies Below' => '0870',
            'What\'s Mine is Yours' => '1110',
            'While Guthix Sleeps' => '0967',
            'Witch\'s House' => '0172',
            'Within the Light' => '1022',
            'Wolf Whistle' => '0918',
            'The World Wakes' => '1114',
            'Zogre Flesh Eaters' => '0504',
            "cool new quest" => '00023',
        );
        $runehqid_to_questname = array();
        //If the quests are not loaded yet, load them now!
        if (array() == $this->quests) {
            $this->quests = $this->Get_Quests_From_DB("que_title");
        }

        // Reverse the map of questname -> id to id -> questname
        foreach ($questname_to_runehqid as $questname => $id) {
            $runehqid_to_questname[$id] = $questname;
        }
        if (!isset($questname_to_runehqid[$quest])) {
            // If the quest is not in the "config", return an empty array
            return array();
        }
        $runehq_url = $this->rs_api->RUNEHQ_URL . "/guide.php?type=quest&id=" . $questname_to_runehqid[$quest];

        // Load data from RuneHQ
        $data = $this->rs_api->API_Request($runehq_url);
        $rhq_quests_needed = array();

        // RegEx out the quest requirements
        preg_match('/<div class="undersubheader">Quest Requirements:<br \/><\/div><div class="guide">(.*?<\/a>)<\/div>/s', $data, $quest_reqs);
        if (isset($quest_reqs[1])) {
            // Split the quests on br
            $req_quests = preg_split("/<br \/>/", $quest_reqs[1]);
            // Go through all the quests and map the quest to a system id
            foreach ($req_quests as $i => $val) {
                preg_match('/id=(\d+)/', $val, $rhq_quests_needed[$i]);
                if(!isset($this->quests[$runehqid_to_questname[$rhq_quests_needed[$i][1]]])){
                    // TODO : Log / Notify us!
                }
                else{
                    $rhq_quests_needed[$i] = $this->quests[$runehqid_to_questname[$rhq_quests_needed[$i][1]]];
                }
            }
        }
        else{
            echo "<h2>Cannot find Quest requirements for $quest</h2>";
        }

        // RS ID skill -> Name
        $skill_map = array(
            0 => "Attack",
            1 => "Defence",
            2 => "Strength",
            3 => "Constitution",
            4 => "Ranged",
            5 => "Prayer",
            6 => "Magic",
            7 => "Cooking",
            8 => "Woodcutting",
            9 => "Fletching",
            10 => "Fishing",
            11 => "Firemaking",
            12 => "Crafting",
            13 => "Smithing",
            14 => "Mining",
            15 => "Herblore",
            16 => "Agility",
            17 => "Thieving",
            18 => "Slayer",
            19 => "Farming",
            20 => "Runecrafting",
            21 => "Hunter",
            22 => "Construction",
            23 => "Summoning",
            24 => "Dungeoneering",
            25 => "Divination",
            26 => "Invention",
        );
        // Reverse RS ID skill -> Name to Name -> ID
        $skill_to_id_map = array();
        foreach ($skill_map as $id => $skill) {
            $skill_to_id_map[$skill] = $id;
        }

        // RegEx out th eskill reqirements
        $skills = array();
        preg_match('/<div class="undersubheader">Skill\/Other Requirements:<br \/>(.*?<\/a>)<br \/><\/div>/s', $data, $skill_reqs);
        if (isset($skill_reqs[1])) {
            // Go through all the quest requirements
            $req_skills = preg_split("/<br \/>/", $skill_reqs[1]);
            foreach ($req_skills as $val) {
                // Get the level (0) and the skill
                preg_match('/(\d+) <a.*?>(\w+)<\/a>/', $val, $skill);
                // Push the result into the $skills array with skill being the RS ID
                array_push($skills, array("skill" => $skill_to_id_map[$skill[2]], "level" => $skill[1]));
            }
        }
        else{
            echo "<h2>Cannot find Skill requirements for $quest</h2>";
        }
        return array("quests" => $rhq_quests_needed, "skills" => $skills, "other" => array());

    }

    /**
     * This function is purely to sync up the data from the database with the data from the API
     * so we can always display all the quests if necessary
     */
    public function Sync_Quests_With_DB()
    {
        if(!isset($this->CI->player)){
            return;
        }
        
        $quests = $this->CI->player->Get_Quests();

        # First select all Quests in DB
        $query = $this->CI->db->get("quests");
        $db_quests = array();
        foreach ($query->result() as $row) {
            $db_quests[$row->que_title] = 1; # Set something as a value so we know the entry is present
        }

        print "<pre>";
        print_r($quests);
        print "</pre>";
        $missing_counter = 0;
        $to_insert = array();
        # Now check if all the quests from the API are in the DB
        foreach ($quests as $i => $val) {
            if (!isset($db_quests[$quests[$i]["title"]])) {
                # Quest is not in DB!
                # TODO Log? Send Mail to us?
                # Import the quest into the DB!
                # TODO Import quest requirements? Cannot do automatically because no "legit" source?
                $data = array(
                    "que_title" => $quests[$i]["title"],
                    "que_qp" => $quests[$i]["questPoints"],
                    "que_diff" => $quests[$i]["difficulty"],
                    "que_is_mem" => $quests[$i]["members"],
                );
                array_push($to_insert, $data);
                $missing_counter++;
            }
        }
        print "MISSING: $missing_counter<br>";
        if ($missing_counter > 0) {
            # There are quests missing! Insert into DB
            # TODO Mail to us so we can add length?
            $this->CI->db->insert_batch("quests", $to_insert);
        }
    }


}
