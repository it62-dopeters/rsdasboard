<?php

class Activity extends Player {
    private $activities = array();
    
    public function Get($val){
        return $this->$val;
    }
    /**
     * @param string $user Username of player
     * @param int $activities Amount of activities you want to load
     * @param array $jsonData JSON Data parsed from RS API
     */
    public function Load($user = '', $activities = 0, $json_data = null)
    {
        if ($user == '') {$user = $this->name;}
        if ($activities > 20) {
            // Maximum 20 activities loadable
            $activities = 20;
        }
        if (!isset($json_data)) {
            $data = $this->CI->rs_api->API_Request($this->CI->rs_api->SKILL_URL . "?user=$user&activities=$activities");
            $json_data = json_decode($data, true);
        }
        if (isset($json_data["activities"])) {
            $this->activities = $json_data["activities"];
        }
/*
{
"activities":[
{
"date" => "05-Feb-2018 21:55",
"details" => "I have capped at my Clan Citadel this week.",
"text" => "Capped at my Clan Citadel."
},
...
]
}
 */
        for ($i = 0; $i < count($this->activities); $i++) {
            $text = $this->activities[$i]["text"];
            if (preg_match('/^Quest complete/', $text)) {
                $type = "Quest completed";
            } elseif (preg_match('/^I found.*?pet/', $text)) {
                $type = "Pet Drop";
            } elseif (preg_match('/^\d+XP/', $text)) {
                $type = "XP Milestone";
            } elseif (preg_match('/^Levelled up/', $text)) {
                $type = "Level-Up";
            } elseif (preg_match('/^I found/', $text)) {
                $type = "Rare Drop";
            } elseif (preg_match('/^I killed/', $text)) {
                $type = "Boss Kill";
                // TODO / NOTE: This is not always accurate! It also tracks kills from Mith Dragons etc.
            } elseif (preg_match('/^Capped at/', $text)) {
                $type = "Capped";
            } elseif (preg_match('/^Visited my/', $text)) {
                $type = "Visited Citadel";
            } elseif (preg_match('/^I defeated/', $text)) {
                $type = "Boss Kill"; // Correct? QBD kills output this!
            } else {
                $type = "Unknown";
            }
            $this->activities[$i]["type"] = $type;
        }
        $this->activities;
    }

}