<?php

class Quest extends Player {
    private $quests = array();
    private $db_quests = array();
    private $quest_reqs = array();
    
    public function Get($val){
        return $this->$val;
    }

    /**
     * @param string $user Username of the user you want to look up
     * @return objects internal quest array
     */
    public function Load()
    {
        if(!isset($this->CI->player->name)){
            # User not defined? Then return nothing
            return;
        }
        $id_to_status_mapper = array(
            1 => "COMPLETED",
            2 => "STARTED",
            3 => "STARTABLE",
            4 => "NOT_STARTABLE"
        );
        # We're gonna load the data from the DB
        $this->CI->db->select("*");
        $this->CI->db->from("player_questprogress");
        $this->CI->db->where("pqp_user", strtolower($this->CI->player->name));
        $this->CI->db->order_by("pqp_insert_date DESC");
        $this->CI->db->limit(1);
        $query = $this->CI->db->get();

        $quests = array();
        foreach ($query->result() as $row) {
            $quests = json_decode($row->pqp_data, true);
        }

        $this->CI->db->select("*");
        $this->CI->db->from("quests");
        $this->CI->db->order_by("que_title");
        $query = $this->CI->db->get();
        foreach ($query->result() as $row) {
            $this->db_quests[$row->que_id] = json_decode(json_encode($row), true);
        }
        # For compatiblity we have to form this data into the values
        # into the same way we get them from the API

        # Format of API
        /*
        Array (
        [title] => A Rum Deal
        [status] => COMPLETED
        [difficulty] => 2
        [members] => 1
        [questPoints] => 2
        [userEligible] => 1
        )
        */

        $return = array(
            "quests" => array()
        );
        foreach($quests as $id => $status){
            $data = array(
                "title" => $this->db_quests[$id]["que_title"],
                "difficulty" => $this->db_quests[$id]["que_diff"],
                "members" => $this->db_quests[$id]["que_is_mem"],
                "questPoints" => $this->db_quests[$id]["que_qp"],
            );
            # Now determine the status
            if($status < 3 && isset($status)){
	        # Qust is completed or started
                $data["status"] = $id_to_status_mapper[$status];
                $data["userEligible"] = 1;
            }
            else{
                $data["status"] = "NOT_STARTED";
                if($status == 3){
                    # Startable
                    $data["userEligible"] = 1;
                }
                else{
                    # NOT Startable
                    $data["userEligible"] = 0;
                }
            }
            array_push($return["quests"], $data);
        }
        foreach ($return["quests"] as $i => $val) {
            $this->quests[$return["quests"][$i]["title"]] = $return["quests"][$i];
        }
    }

    public function Load_From_API(){
        if(!isset($this->CI->player->name)){
            # User not defined? Then return nothing
            return;
        }
        if (!isset($json_data)) {
            $data = $this->CI->rs_api->API_Request($this->CI->rs_api->QUEST_URL . "?user=" . $this->CI->player->name);
            $json_data = json_decode($data, true);
        }
        /*
        Quest structure:
        Array (
        [title] => A Rum Deal
        [status] => COMPLETED
        [difficulty] => 2
        [members] => 1
        [questPoints] => 2
        [userEligible] => 1
        )
         */
        foreach ($json_data["quests"] as $i => $val) {
            $this->quests[$json_data["quests"][$i]["title"]] = $json_data["quests"][$i];
        }

    }

    public function Load_From_DB($key = "que_id"){
        $query = $this->CI->db->get("quests");
        foreach ($query->result() as $row) {
            $this->db_quests[$row->$key] = json_decode(json_encode($row), true);
        }
        return $this->db_quests;
    }

    public function Load_Requirements_From_DB(){
        # Then load the quest with given quest id from DB
        $this->CI->db->select('*');
        $this->CI->db->from('quests');
        $this->CI->db->join('questrequirements', 'que_id = qre_que_id');
        $query = $this->CI->db->get();

        foreach ($query->result() as $row) {
            if(!isset($this->quest_reqs[$row->que_id])){
                $this->quest_reqs[$row->que_id] = array(
                    "skills" => array(), # skills
		    "quests" => array(), # quests
		    "other" => array(), # other reqs
                );
            }
            if($row->qre_type == "quest"){
                array_push($this->quest_reqs[$row->que_id]["quests"], $row->qre_name);
            }
            elseif($row->qre_type == "skill"){
                array_push($this->quest_reqs[$row->que_id]["skills"], array("skill" => $row->qre_name, "level" => $row->qre_value));
	    }
	    elseif($row->qre_type == "other"){
		    array_push($this->quest_reqs[$row->que_id]["other"], array("name" => $row->qre_name, "value" => $row->qre_value));
	    }
	}
    }

    public function Load_Requirement($que_id, $skills){
        if(!isset($que_id) || !isset($this->quest_reqs[$que_id]) || $skills == array()){ return array( array(), array() ); }
        $result = array(
            array(), # Skills
            array(), # Quests
    );
	foreach($this->quest_reqs as $qre_que_id => $req){
		if($que_id != $qre_que_id){
			continue;
		}
            foreach($req["quests"] as $quest){
                $color = '';
                switch($this->quests[$quest]["status"]){
                    case "COMPLETED":
                        $color = "#0bd00a";
                        break;
                    case "STARTED":
                        $color = "yellow"; 
                        break;
                    case "NOT_STARTED":
                        $color = "red";
                        break;
                    default:
                        $color = "white";
                        break;
                }
                array_push($result[1], array(
                    "name" => $quest,
                    "statusColor" => $color
                ));
            }
            foreach($req["skills"] as $s_req){
                array_push($result[0], array(
                    "name" => $s_req["level"] . " " . $s_req["skill"],
                    "statusColor" => ($skills[$s_req["skill"]]["level"] < $s_req["level"] ? "red" : "#0bd00a")
                ));
	    }
	    foreach($req["other"] as $s_req){
			array_push($result[0], array(
				"name" => $s_req["name"] . ": " . $s_req["value"],
				"statusColor" => 'grey'
			));
            }


        }
        return $result;
    }

    /**
     * @param string $status Status of the quest [States can be COMPLETED, STARTED, NOT_STARTED ]
     * @return quest array with RS API structure
     */
    public function Get_Quests_By_Status($status = 'COMPLETED')
    {
        $quests = array();
        for ($i = 0; $i < count($this->quests); $i++) {
            $cur_quest = $this->quests[$i];
            if ($cur_quest["status"] == $status) {
                array_push($quests, $cur_quest);
            }
        }
        return $quests;
    }

    /**
     * @return quest array with RS API structure
     */
    public function Get_Startable_Quests()
    {
        $quests = array();
        for ($i = 0; $i < count($this->quests); $i++) {
            $cur_quest = $this->quests[$i];
            if ($cur_quest["userEligible"] == 1 && $cur_quest["status"] == "NOT_STARTED") {
                array_push($quests, $cur_quest);
            }
        }
        return $quests;
    }

    public function Get_Quest_By_Name($name = ''){
        if($name === ""){return array();}
        foreach ($this->quests as $name => $quest) {
            if ($quest["title"] == $name){
                return $quest;
            }
        }
    }


}
