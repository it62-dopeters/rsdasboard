<?php
class Skill extends Player {
    private $skills = array();

    public function Get($val){
        return $this->$val;
    }
    /**
     * @param string $user Username of player
     * @param array $json_data JSON Data parsed from RS API
     * @return skill array with RS API structure
    */
    public function Load($user = '', $json_data = null)
    {
        if ($user == '') {$user = $this->name;}
        if (!isset($json_data)) {
            $data = $this->CI->rs_api->API_Request($this->CI->rs_api->SKILL_URL . "?user=$user&activities=0");
            $json_data = json_decode($data, true);
        }
        // If the profile could not be loaded
        if (isset($json_data["error"])) {
            return $json_data["error"];
        }
        $this->skills = array(); // Empty current skill array if it exists
        /*
        Response:
        {
        name => Landwirtin,
        activities => [ ... ],
        skillvalues => [
        {
        level => 107,
        xp => 314658360,
        rank => 123234,
        id => 24
        },
        ],
        }
         */
        for ($i = 0; $i < count($json_data["skillvalues"]); $i++) {
            # Accessing by: $this->skills["Attack"]
            $this->skills[SKILL_ID_TO_NAME[$json_data["skillvalues"][$i]["id"]]] = $json_data["skillvalues"][$i];
        }
        return $this->skills;
    }

}