<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Graph_Model extends CI_Model
{
	public function GenerateGraphData($user){
		$data = array();
		$data["skillbuttons"] = '';
		$data["skilldata"] = '';
		
		
		$dates = array();
		$max=7;
		for($i=0; $i<$max; $i++){
			$datum = date("Y-m-d", time() - (60*60*24*($max-1-$i)));
			$dates[$datum]=$i;
		}
		# first load the latest skill data so we don't insert unnecessary trash
		$this->db->select("pgs_exp, date(pgs_insert_date) AS pgs_insert_date, pgs_skill ");
		$this->db->from('player_skillprogress');
		$this->db->where("pgs_user", $user);
		$this->db->where("pgs_insert_date > NOW() - INTERVAL 6 DAY");
		$this->db->order_by("pgs_insert_date DESC");
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$index = $dates[$row->pgs_insert_date];
			if(!isset($index)){
				continue; # Date is not in date array?! skip it!
			}
			if(isset($result[$row->pgs_skill][$index])){
				continue; # If there is alredy a value, do not overwrite it
			}
			if(!isset($result[$row->pgs_skill])){
				$result[$row->pgs_skill] = array();
			}
			$result[$row->pgs_skill][$index] = $row->pgs_exp;
		}
		if(isset($result)){
			foreach($result as $skill => &$val){
				do{
					for($j=0;$j<$max;$j++){
						if(!isset($val[$j]) && isset($val[$j-1])){
							$val[$j] = $val[$j-1];
						}
						else if(!isset($val[$j]) && isset($val[$j+1])){
							$val[$j] = $val[$j+1];
						}
					}
				}
				while(count($val) < $max);
			}
		}
		
		foreach(SKILL_ID_TO_NAME as $index => $skill){
			$data["skillbuttons"] .= '<div id="skill-' . $skill . '" class="col-sm-2 col-xl-1 skill-button skill-hidden" style="background-image: url(\'' . base_url("css/images/$skill\.png") . '\');"></div>';
			$data["skilldata"] .= '"' . $skill . '": {
				data: [' . implode(",", 
					(isset($result[$index]) ? array_reverse($result[$index]) : array()))
				       	. '],
				backgroundColor: window.chartColors.blue,
				borderColor: window.chartColors.blue,
			},';

		}
		return $data;
	}
}
