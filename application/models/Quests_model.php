<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Quests_Model extends CI_Model
{
    # This is just debug data and should NOT be used for production!!
    public function Create_Offline_Quest_Table(){
        return "<div class='table table--4cols'>" . $this->Create_Quest_Row(
            array(
                "Testquest", 123, "Hard", "???"
            ),
            2,
            "COMPLETED",
            array(
                array(
                    array(
                        "name" => "78 Summoning",
                        "statusColor" => "red",
                    )
                ),
                array(
                    array(
                        "name" => "The curse",
                        "statusColor" => "orange",
                    )
                )
            )
        ) . "<div class='table table--4cols'>" . $this->Create_Quest_Row(
            array(
                "Testquest #2", 123, "Hard", "???"
            ),
            1,
            "STARTED",
            array(
                array(
                    array(
                        "name" => "44 Divination",
                        "statusColor" => "red"
                    ),
                    array(
                        "name" => "68 Hunter",
                        "statusColor" => "green",
                    )
                ),
                array(
                    array(
                        "name" => "Prif",
                        "statusColor" => "green"
                    ),
                    array(
                        "name" => "Shield of Arrav",
                        "statusColor" => "green"
                    )
                )
            )
        

        );
    }

    public function Create_Quest_Row($data, $id = 0, $status = '', $reqs = array())
    {
        $style = "class='table-cell table-cell--$status quest'";
        $tr = "<div class='table table--4cols'>";
        foreach ($data as $value) {
            $tr .= "<div id='quest-$id' $style>$value</div>";
        }
        $tr .= "<div style='width: 100%; display:none;' id='reqs-$id' class='table--2cols'>";
        $headings = array(
            "Skills required",
            "Quests required",
        );
        if($reqs !== array()){
            for($i=0;$i<2;$i++){
                $req = $reqs[$i];
                $tr .= "<div class='table-cell table-cell--$status'>";
                if(array() === $req){
                    $req = array(array("statusColor" => "white", "name" => "None"));
                }
                $head = $headings[$i];
                # There are requirements
                $tr .= "<h3>$head</h3>";
                foreach($req as $r){
                    $tr .= "<p style='color:" . $r['statusColor'] . "'>" . $r['name'] . "</p>";
                }
                
                $tr .= "</div>";
            }
        }
        $tr .= "</div></div>";
        return $tr;
    }

    public function Get_Quest_List($name = '')
    {
        $table_data = "";
        $this->load->library("Player", array("name" => $this->user->getUsername()));
        $this->player->Init();
        $this->quest->Load_From_DB("que_title");
        $this->quest->Load_Requirements_From_DB();
        $db_quests = $this->quest->Get("db_quests");
        $quests = $this->quest->Get("quests");
        $skills = $this->skill->Get("skills");
        # Table head
        $table_data .= $this->Create_Quest_Row(array(
            "Title",
            "Quest Points",
            "Difficulty",
            "Length"
        ), 0, "header");

        foreach ($quests as $name => $quest) {
            $status = '';
            if ($quest["status"] == "COMPLETED") {
                $status = "completed";
            } elseif ($quest["status"] == "STARTED") {
                $status = "started";
            } elseif ($quest["status"] == "NOT_STARTED") {
                # Now check if the user can start the quest or not
                if ($quest["userEligible"] == 1) {
                    # User could start the quest
                    $status = "not-started";
                } else {
                    # User could not start the quest
                    $status = "not-startable";
                }
	    }
            $table_data .= $this->Create_Quest_Row(
                array(
                    $quest["title"], 
                    $quest["questPoints"], 
                    QUEST_DIFFICULTY_MAP[$quest["difficulty"]]["name"], 
                    $db_quests[$quest["title"]]["que_length"]
                ),
                $db_quests[$quest["title"]]["que_id"],
                $status,
		$this->quest->Load_Requirement($db_quests[$quest["title"]]["que_id"], $skills)
                #$this->Load_Quest_Requirements_From_DB($index, $this->player)
            );
        }
        return $table_data;
    }

    public function Fetch_Quest_Requirements($quest){
	$CI =& get_instance();
    	$wiki_url = 'http://runescape.wikia.com/wiki/';
	$req_url = $wiki_url . $quest;
	$req_url = str_replace(" ", "_", $req_url);
	if(!isset($CI->rs_api)){
		return "Need instance of rs_api"; # We need an instanced API Object in order to make requests
	}
	print "Frage ab: $req_url\n";
	
	$data = $CI->rs_api->RAW_Request($req_url);
	
	# Wenn eine Questrequirements da ist dann gibts das hier:
	# <table class="mw-collapsible mw-collapsed questreq" style="background:none;">
	# Ansonsten nicht..

	$quest_reqs = array();
	preg_match('/<table class=".*?questreq".*?>(.*?)<\/table>/s', $data, $quest_reqs);

	print_r($quest_reqs[1]);
	$quests = $quest_reqs[1];
	# Es gibt die Moeglichkeit, das nur eine Quest benoetigt wird oder mehrere
	#
	# Mehrere Quests:
	$quest = array();
	preg_match('/<ul>(.*?)<\/ul>/', $quests, $quest);

	print "<br>";
	print_r($quest);
	# Wenn es Items gibt, die notwendig sind, dann gibts das hier:
	# <tr><th class="questdetails-header">Items required</th><td class="questdetails-info">...</td></tr>
	# Ansonsten nicht..
#	print $data;

    }
}
