
<div class="chart-container" style=" position:relative; height:60vh; width:80vw;">
	<canvas id="canvas"></canvas>
</div>
<br>
<br>

<div id="skill_graph_options">

	<?php echo $skillbuttons; ?>
</div>
<script src="<?php echo base_url("vendor/chart.js/Chart.min.js") ?>"></script>
<script src="<?php echo base_url("vendor/chart.js/utils.js") ?>"></script>

<script>
	Chart.defaults.global.legend.display = false;
	Chart.defaults.global.defaultFontColor = "#fff";
	window.onload = function() {
		var config = {
			type: 'line',
			data: {
				labels: ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5', 'Day 6', 'Day 7'],
				datasets: []
			},

			options:
			{
				maintainAspectRatio: false,
				responsive: true,
				title:
				{
					display: true,
					text: 'Chart.js Line Chart'
				},

				tooltips:
				{
					mode: 'index',
					intersect: false,
				},

				hover:
				{
					mode: 'nearest',
					intersect: true
				},

				elements:
				{
					line:
					{
						tension: 0, //disables bezier curves
					},
				},
				    legend: {
                    display: false,
                },

				scales:
				{
					xAxes: [
					{
							display: true,
							stacked: true,
							gridLines: {
							color: "#FFFFFF"
						},

						scaleLabel:
						{
							display: true,
							labelString: 'Last 7 Days'
						}
					}
					],

					yAxes: [
					{
						display: true,
						stacked: false,
						gridLines:
						{
						color: "#FFFFFF"
						},

						scaleLabel:
						{
							display: true,
							labelString: 'Experience'
						}
					}
					]
				}
			}
		};
		var can = document.getElementById('canvas');
		var ctx = can.getContext('2d');

		window.myLine = new Chart(ctx, config);
	};
	var graph_options = document.getElementById("skill_graph_options");
	for(var i=0; i < graph_options.childNodes.length; i++){
		var option = graph_options.childNodes[i];
		option.addEventListener("click", function(e) {
			// Is the current class hidden? If so, make it visible, otherwise change from visible to hidden
			this.className = (this.className.match(/hidden/) ? this.className.replace("hidden", "visible") : this.className.replace("visible", "hidden"));
			// Re-Load the Graph
			Reload_Graph();
		});
	}
	var skill_data = { <?php echo $skilldata ?> }; //This comes from PHP once implemented

	function Reload_Graph(){
		var data_to_display = [];
		var graph_options = document.getElementById("skill_graph_options");
		for(var i=0; i < graph_options.childNodes.length; i++){
			var option = graph_options.childNodes[i];
			if(option.toString() == "[object Text]"){continue;}
			if(option.className.match(/visible/)){
				var data = skill_data[option.id.slice(6)];
				data.fill=false;
				data.label=option.id.slice(6);
				data_to_display.push(data);
			}
		}
		window.myLine.data.datasets = data_to_display;
		window.myLine.update();
	}
</script>
