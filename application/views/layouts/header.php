<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>RS Dashboard</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url() ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>css/sb-admin.css" rel="stylesheet">
  <!-- Responsive Tables (Custom) -->
  <link href="<?php echo base_url() ?>css/responsive-tables.css" rel="stylesheet">
  <!-- Google Font "cinzel" -->
  <link href='https://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet'>
</head>

<style>
body {
    font-family: 'Cinzel';
}
</style>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">

    <a class="navbar-brand" href="<?php echo base_url() ?>"><img class="branding" src="<?php echo base_url() ?>css/images/branding.png">Dashboard</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">

      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item">
        		<img class="dash-avatar" src="<?php echo base_url() ?>css/images/avatar.png">
        		<img class="dash-clan" src="<?php echo base_url() ?>css/images/ownclan.png">
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
          <a class="nav-link" href="<?php echo base_url() ?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">HOME</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Quests">
          <a class="nav-link" href="<?php echo base_url() ?>quests">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">QUESTS</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Skillprogress">
          <a class="nav-link" href="<?php echo base_url() ?>graph">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">SKILLPROGRESS</span>
          </a>
        </li>
      </ul>

      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <?php
          echo '<a class="nav-link" href=\'';
          if($this->session->userdata('isUserLoggedIn')){
           echo base_url("users/logout") . "'>";;
           echo '<i class="fa fa-fw fa-sign-out"></i>Logout</a>';
          }
          else{
            echo base_url("users/login") . "'>";;
            echo '<i class="fa fa-fw fa-sign-out"></i>Login</a>';

          }
          ?>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper main-bg-c">
    <div class="container-fluid">
