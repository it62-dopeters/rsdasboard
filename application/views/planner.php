<div id="main">
<div id="dailies" class="col-xl-7">
    <button id='add_daily'>Add daily</button>
    <div id="daily_editors">
    </div>
</div>
<div id="daily_overview" class="col-xl-5">
<form method="POST" id="daily_form" action="planner/add">
<input type="hidden" name="dailies" id="daily_config"/>
<input type="submit" value="Submit">
</form>
</div>
</div>

<script>
daily_config = {
	"dailies": [],
};
var added_dailies=0;
document.getElementById("add_daily").addEventListener("click", function(){
	document.getElementById("daily_editors").innerHTML += `
		<div id='add_${added_dailies}' class='container col-xl-12'>
			<label class='col-xl-5' for='#add_daily_${added_dailies}'>
				Name of daily
			</label>
			<input style='padding: 0' class='col-xl-6' id='add_daily_${added_dailies}' type='text' placeholder='...'>
			<label class='col-xl-5' for='#add_number_${added_dailies}'>
				Repetitions/ day
			</label>
			<input style='padding: 0.5em 0' class='col-xl-6' id='add_number_${added_dailies}' type="number" value=1>
			<button id='add_button_${added_dailies}' onclick="AddDaily(this)">Add</button>
		</div>`;
	added_dailies++;
});
function AddDaily(button){
	var id=0;
	if(button.id === undefined){
		return;
	}
	else{
		var id_match = button.id.match(/(\d+)/);
		if(id_match){
			id=id_match[1];
			var add_div  = document.getElementById("add_" + id);
			var add_name = document.getElementById("add_daily_" + id).value;
			var add_rep  = document.getElementById("add_number_" + id).value;
			
			daily_config.dailies.push({
				"name": add_name,
				"repetitions": add_rep 
			});
			// Now that we have the input data, let's create an actual form
			document.getElementById("daily_config").value = JSON.stringify(daily_config);
			// TOOD prettify output
			document.getElementById("daily_form").innerHTML += `<p>${add_name};${add_rep}</p>`;
			// Remove form
			document.getElementById("add_" + id).remove();
		}
	}
	return;

}
</script>
