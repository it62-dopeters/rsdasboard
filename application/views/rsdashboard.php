	  <!-- Icon Cards-->
	  <script src="<?php echo base_url("vendor/dist/progressbar.js") ?>"></script>
     	<div class="row">

        	<div class="col-xl-3 col-sm-6 mb-3">
          		<div class="card text-white o-hidden h-100 bg-img-main">
            		<div class="card-body">
              			<div class="dash-card-text">Goldberg Machine</div>
              				<div class="row">
	              				<div class="col-xl-6">
	              					<div class="middle"><a> First Rune </a></div>
	              					<img class="rune-img" src="<?php echo base_url() ?>/css/images/Blood_rune.png"  height="59" width="59">
	              				</div>
	              				<div class="col-xl-6">
	              					<div class="middle"><a> Second Rune </a></div>
	              					<img class="rune-img" src="<?php echo base_url() ?>/css/images/Nature_rune.png" height="59" width="59"> 
	              				</div>
	            			</div>
            		</div>
        		</div>
        	</div>

        	<div class="col-xl-3 col-sm-6 mb-3">
          		<div class="card text-white o-hidden h-100 bg-img-main">
            		<div class="card-body">
              			<div class="dash-card-text">Voice of Seren</div>
              				<div class="row">
	              				<div class="col-xl-6">
	              					<img class="card-img" src="<?php echo base_url() ?>/css/images/Cadarn_Clan.png"  height="59" width="59">
	              				</div>
	              				<div class="col-xl-6">
	              					<img class="card-img" src="<?php echo base_url() ?>/css/images/Trahaearn_Clan.png" height="59" width="59"> 
	              				</div>
	            			</div>
            		</div>
        		</div>
        	</div>

        	<div class="col-xl-3 col-sm-6 mb-3">
          		<div class="card text-white o-hidden h-100 bg-img-main">
            		<div class="card-body">
              			<div class="dash-card-text">Questpoints</div>
              				<div class="row">
	              				<div class="col-xl-6">
	              					<img class="card-img questicon" src="<?php echo base_url() ?>/css/images/Questicon.png"  height="59" width="59">
	              				</div>
	              				<div class="col-xl-6">
	              					<div>
	              						<a class="qp"> 184 </a>
	              					</div>
	              				</div>
	            			</div>
            		</div>
        		</div>
        	</div>

        	<div class="col-xl-3 col-sm-6 mb-3">
          		<div class="card text-white o-hidden h-100 bg-img-main">
            		<div class="card-body">
              			<div class="dash-card-text">Card 4</div>
              				<div class="row">
	              				<div class="col-xl-6">
	              					<div class="middle"><a> First Rune </a></div>
	              					<img class="card-img" src="<?php echo base_url() ?>/css/images/Slayer.png"  height="59" width="59">
	              				</div>
	              				<div class="col-xl-6">
	              					<div class="middle"><a> Second Rune </a></div>
	              					<img class="card-img" src="<?php echo base_url() ?>/css/images/Farming.png" height="59" width="59"> 
	              				</div>
	            			</div>
            		</div>
        		</div>
        	</div>        	        	        		
      </div>
    	<!-- Skill Overview -->

      <?php echo $skill_ov;?> 

                          <script> 
                    	var bars = document.getElementsByClassName("progress-bar-row");
						for(var i=0;i<bars.length;i++){
						var el = bars[i];
						var bar =  new ProgressBar.Line("#" + el.id, {
                          strokeWidth: 4,
                          easing: "easeInOut",
                          duration: 2800,
                          color: "#FFEA82",
                          trailColor: "#515559",
                          trailWidth: 4,
                          from: {color: "#A80000"},
                          to: {color: "#1EEF0B"},
                          step: (state, bar) => {
                            bar.path.setAttribute("stroke", state.color);
                          }
                		});
							if(el.dataset.percent == 0.00){
							bar.animate(0.01);
							}
							else{
							bar.animate(el.dataset.percent);
							}
						}
                    </script>
