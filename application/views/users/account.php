<!DOCTYPE html>
<html lang="en">  
<head>

</head>
<body>
<div class="container">
    <h2>User Account</h2>
    <h3>Welcome <?php echo $user['username']; ?>!</h3>
    <div class="account-info">
        <p><b>Name: </b><?php echo $user['username']; ?></p>
        <p><b>Email: </b><?php echo $user['email']; ?></p>	
        <a class="btn btn-default" href="/index.php/users/logout">Logout</a>
    </div>
</div>
</body>
    <script src="<?php echo base_url() ?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>vendor/datatables/dataTables.bootstrap4.js"></script>
</html>